#!/bin/bash

# Set variables ----------------------------------------------------
export devenv_file="<INFO_DIR>/provisioned/provisioned-$(date +"%Y-%m-%d-%H-%M")"
export devenv_info="<INFO_FILE_PATH>"
# ------------------------------------------------------------------

# Create the info file
touch $devenv_file

# File creation info
echo " -- Created on $(date +"%Y-%m-%d-%H-%M") by user $(whoami)" | tee -a $devenv_file

echo "" | tee -a $devenv_file
echo "-- DEVELOPMENT ENVIRONMENT SETTINGS ----------------------------------------" | tee -a $devenv_file
echo "-> stored in $devenv_info at the time of last provisioning" | tee -a $devenv_file

cat $devenv_info | tee -a $devenv_file

echo "" | tee -a $devenv_file
echo "-- CURRENT INFORMATION ------------------------------------------------------" | tee -a $devenv_file

#Write Centos version
yum list installed centos-release | grep centos-release | awk -F " " '{print "Centos version: " $2}' | awk -F \. '{print $1"."$2}' | tee -a $devenv_file

# Obtain the version of Java
echo "Java version: $(java -version 2>&1 >/dev/null | grep 'java version' | awk '{print $3}' | awk -F \" '{print $2}'
)" | tee -a $devenv_file

# Obtain CSS version
echo "CS-Studio version: $(basename /opt/cs-studio/features/org.csstudio.product.feature_* | awk -F "product.feature_" '{print $2}')" | tee -a $devenv_file

# Obtain Ipython version
echo "Ipython version: $(ipython --version)" | tee -a $devenv_file

# Write EEE build system version
echo $EPICS_ENV_PATH | awk -F/ '{print "EEE build system " $6}' | tee -a $devenv_file

# Write EEE runtime version
which iocsh | awk -F/ '{print "EEE runtime version " $6}' | tee -a $devenv_file
                     
# List available repositories
(echo "" && echo "Repositories:" && yum repolist | grep -v "Loaded plugins" | grep -v "Loading mirror" | grep -v "repo id" | grep -v "repolist:") | tee -a $devenv_file

# List installed Kernel rpms
(echo "" && echo "Installed Kernel rpms:" && yum list installed "kernel*" | grep kernel) | tee -a $devenv_file

echo " " | tee -a $devenv_file
echo "-- Output saved to file $devenv_file"


