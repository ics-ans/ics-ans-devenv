Development Environment version: <DEVENV_VERSION>

Software installed by playbooks in the last provisioning:
SSSD: <DEVENV_SSSD>
EEE source: <DEVENV_EEE>
NFS server: <NFS_SERVER>
CSS: <DEVENV_CSS>
OpenXAL: <DEVENV_OPENXAL>
Ipython Notebook: <DEVENV_IPYTHON>
