---
- name: Ensure CSS folder exists to avoid non-existing content errors
  file:
    path: "{{ CSS_DIR }}"
    state: directory

- name: Ensure version file exists to avoid non-existing content errors
  file:
    path: "{{ VERSION_FILE }}"
    state: touch

- name: Get version from the version file
  shell: "cat {{ VERSION_FILE }}"
  register: current_version

- set_fact: CSS_OVERWRITE="false"
  when: "'{{ current_version.stdout }}' == '{{ CSS_VERSION }}'"
#    when: "'{{ lookup('file', VERSION_FILE) }}' == '{{ CSS_VERSION }}'" For some reason does not work in Docker containers

- debug:
    msg: "Installed version is {{ current_version.stdout }}, new version is {{ CSS_VERSION }}"

- debug:
    msg: "CSS will not be overwritten since the new version matches the installed one."
  when: "{{ CSS_OVERWRITE }} == false"

- debug:
    msg: "CSS will be installed/overwritten since it does not exist yet or since the new version upgrades the installed one."
  when: "{{ CSS_OVERWRITE }} == true"

# Unarchive module unable to obtain the file from the internet, therefore we do it manually

- name: Download packaged cs-studio
  get_url:
    url: "https://artifactory01.esss.lu.se/artifactory/list/CS-Studio/production/{{ CSS_VERSION }}/cs-studio-ess-{{ CSS_VERSION }}-linux.gtk.x86_64.tar.gz"
    dest: "/opt/cs-studio-ess-{{ CSS_VERSION }}-linux.gtk.x86_64.tar.gz"
  when: "{{ CSS_OVERWRITE }} == true"

- name: Remove the old version of css
  file:
    path: /opt/cs-studio
    state: absent
  when: "{{ CSS_OVERWRITE }} == true"

- name: Unpackage cs-studio
  unarchive:
    src: "/opt/cs-studio-ess-{{ CSS_VERSION }}-linux.gtk.x86_64.tar.gz"
    dest: "/opt/"
    copy: no
  when: "{{ CSS_OVERWRITE }} == true"

- name: Remove tarball
  file:
    path: "/opt/cs-studio-ess-{{ CSS_VERSION }}-linux.gtk.x86_64.tar.gz"
    state: absent

- name: Create symlink in /usr/bin/
  file:
    src: "/opt/cs-studio/ESS CS-Studio"
    dest: "/usr/bin/css"
    state: link

- name: Create the ess directory structure
  file:
    path: /ess/.session
    state: directory
    owner: root
    group: root
    mode: 0777

- name: Create the /ess/.session/local_user file so it's writeable by everyone
  file:
    path: /ess/.session/local_user
    state: touch
    owner: root
    group: root
    mode: 0666

############################################ CSS-TOOLS

- name: Create some needed directories
  file:
    path: /tmp/css-tools
    state: directory


- name: Download ess-tools-common from git
  git:
    repo: "https://bitbucket.org/europeanspallationsource/ics-ess-tools-common"
    dest: /tmp/css-tools
    force: yes
    depth: 1
    version: "v{{ CSS_TOOLS_VERSION }}"

- name: Check if localenv service exists
  stat:
    path: /usr/lib/systemd/system/localenv.service
  register: service_status

- name: Stop localenv service
  service:
    name: localenv.service
    state: stopped
  when: service_status.stat.exists

- name: Copy unpackaged files in appropriate locations
  shell: "cp -r /tmp/css-tools/{{ item }}"
  with_items:
    - 'localenv.service /usr/lib/systemd/system/'
    - 'ess-logo-square.png /ess/'
    - 'ESS.directory /usr/share/desktop-directories/'
    - '0.ESS_init_user_configurations.sh /etc/profile.d/'
    - 'desktop/* /usr/share/applications/'
    - 'xulrunner /ess/'

- name: Refresh the service
  shell: systemctl daemon-reload

- name: Remove ess-tools-common git folder
  file:
    path: "/tmp/css-tools"
    state: absent

- name: Deploy css-update
  copy:
    src: css-update
    dest: /usr/local/bin/css-update
    mode: 0755
    owner: root
